<h1>Nuevo Estudiante</h1>
<form class=""
action="<?php echo site_url(); ?>/estudiantes/guardar_Est"
method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Cédula:</label>
          <br>
          <input type="number"
          placeholder="Ingrese la cédula"
          class="form-control"
          name="cedula_est" value=""
          id="cedula_est">
      </div>
      <div class="col-md-4">
          <label for="">Apellidos:</label>
          <br>
          <input type="text"
          placeholder="Ingrese sus apellidos"
          class="form-control"
          name="apellidos_est" value=""
          id="apellidos_est">
      </div>
      <div class="col-md-4">
        <label for="">Nombres:</label>
        <br>
        <input type="text"
        placeholder="Ingrese los nombres"
        class="form-control"
        name="nombres_est" value=""
        id="nombres_est">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Carrera:</label>
          <br>
          <input type="text"
          placeholder="Ingrese lo Carrera"
          class="form-control"
          name="carrera_est" value=""
          id="carrera_est">
      </div>
      <div class="col-md-4">
          <label for="">Ciclo:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el ciclo"
          class="form-control"
          name="ciclo_est" value=""
          id="ciclo_est">
      </div>
      <div class="col-md-4">
        <label for="">Curso:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el curso:"
        class="form-control"
        name="curso_est" value=""
        id="curso_est">
      </div>
    </div>

    <br>
    <!-- <div class="row">
      <div class="col-md-12">
          <label for="">Dirección:</label>
          <br>
          <input type="text"
          placeholder="Ingrese la direccion"
          class="form-control"
          name="direccion_ins" value=""
          id="direccion_ins">
      </div>
    </div> -->
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/estudiantes/index"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>
