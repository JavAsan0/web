<h1>LISTADO DE ESTUDIANTES</h1>
<br>
<?php if ($estudiantes): ?>
  <table class="table table-striped table-bordered">
    <thead>
      <tr>
        <th>ID</th>
        <th>CEDULA</th>
        <th>APELLIDOS</th>
        <th>NOMBRES</th>
        <th>CARRERA</th>
        <th>CICLO</th>
        <th>CURSO</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
  </thead>
  <tbody>
    <?php foreach ($estudiantes as $filaTemporal): ?>
      <tr>
        <td>
          <?php echo $filaTemporal->id_est ?>
        </td>
        <td>
          <?php echo $filaTemporal->cedula_est ?>
        </td>
        <td>
          <?php echo $filaTemporal->apellidos_est ?>
        </td>
        <td>
          <?php echo $filaTemporal->nombres_est ?>
        </td>
        <td>
          <?php echo $filaTemporal->carrera_est ?>
        </td>
        <td>
          <?php echo $filaTemporal->ciclo_est ?>
        </td>
        <td>
          <?php echo $filaTemporal->curso_est ?>
        </td>
        <td class="text-center">
          <a href="#" title="Editar Instructor">
            <i class="glyphicon glyphicon-pencil"></i>
          </a>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <a href="<?php echo site_url("estudiantes/eliminar/$filaTemporal->id_est")?>"title="Eliminar Estudiante" style="color:red">
           <i class="glyphicon glyphicon-trash"></i>
         </a>
       </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
  </table>
<?php else: ?>
  <h1>No hay datos</h1>
<?php endif; ?>
