<?php
  class Instructores extends CI_Controller
  {
    function __construct()
    {
      parent:: __construct();
      //error_reporting(0);

      //Cargar aqui todos los modelos
      $this->load-> model('InstructorModel');
    }
    public function index(){
      $data['instructores']=$this->InstructorModel->obtenerTodos();
      $this->load->view('header');
      $this->load->view('instructores/index',$data);
      $this->load->view('footer');
    }
    public function nuevo(){
      $this->load->view('header');
      $this->load->view('instructores/nuevo');
      $this->load->view('footer');
    }
    public function guardar(){
      $datosNuevoInstructor=array(
        "cedula_ins"=>$this->input->post('cedula_ins'),
        "primer_apellido_ins"=>$this->input->post('primer_apellido_ins'),
        "segundo_apellido_ins"=>$this->input->post('segundo_apellido_ins'),
        "nombres_ins"=>$this->input->post('nombres_ins'),
        "titulo_ins"=>$this->input->post('titulo_ins'),
        "telefono_ins"=>$this->input->post('telefono_ins'),
        "direccion_ins"=>$this->input->post('direccion_ins')
      );
      if($this->InstructorModel->insertar($datosNuevoInstructor)){
        redirect('instructores/index');
      }else{
        echo "<h1>ERROR AL INSERTAR</H1>"; //EMBEBER CODIGO
      }
    }

    //Funcion para eliminar INSTRUCTORES
    public function eliminar($id_ins){
      if ($this->InstructorModel->borrar($id_ins)) {
        redirect('instructores/index');
      } else {
        echo "ERROR AL BORRAR :(";
      }
    }


      //
      //print_r($datosNuevoInstructor);
      // echo $this->input->post('cedula_ins'); //Post es seguro y get es inseguro, se recomienda usar POST, AQUI SE CAPTURA
      // echo "<br>"; //embeber codigo, poner codigo de un tipo en otro... poner html en php
      // $this->input->post('primer_apellido_ins');
      // echo "<br>";
      // echo $this->input->post('segundo_apellido_ins');
      // echo "<br>";
      // echo $this->input->post('nombres_ins');
      // echo "<br>";
      // echo $this->input->post('titulo_ins');
      // echo "<br>";
      // echo $this->input->post('telefono_ins');
      // echo "<br>";
      // echo $this->input->post('direccion_ins');

  }//Cierre de la clase
 ?>
