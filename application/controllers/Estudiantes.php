<?php
  class Estudiantes extends CI_Controller
  {
    function __construct()
    {
      parent:: __construct();
      //error_reporting(0);

      //Cargar aqui todos los modelos
      $this->load-> model('EstudianteModel');
    }
    public function index(){
      $data['estudiantes']=$this->EstudianteModel->obtenerTodos();
      $this->load->view('header');
      $this->load->view('estudiantes/index',$data);
      $this->load->view('footer');
    }
    public function nuevo(){
      $this->load->view('header');
      $this->load->view('estudiantes/nuevo');
      $this->load->view('footer');
    }

    public function guardar_Est(){
      $datosNuevoEstudiante=array(
        "cedula_est"=>$this->input->post('cedula_est'),
        "apellidos_est"=>$this->input->post('apellidos_est'),
        "nombres_est"=>$this->input->post('nombres_est'),
        "carrera_est"=>$this->input->post('carrera_est'),
        "ciclo_est"=>$this->input->post('ciclo_est'),
        "curso_est"=>$this->input->post('curso_est')
      );
      if($this->EstudianteModel->insertar($datosNuevoEstudiante)){
        redirect('estudiantes/index');
      }else{
        echo "<h1>ERROR AL INSERTAR</H1>"; //EMBEBER CODIGO
      }
    }

    //Funcion para eliminar estudiante
    public function eliminar($id_est){
      if ($this->EstudianteModel->borrar($id_est)) {
        redirect('estudiantes/index');
      } else {
        echo "ERROR AL BORRAR :(";
      }
    }
  }//Cierre de la clase
 ?>
