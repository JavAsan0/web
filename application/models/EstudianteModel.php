<?php
  class EstudianteModel extends CI_Model //CI_Model ya viene con el framework
  {
    function __construct()
    {
      // Reconocer a las clases
      parent::__construct();
    }
    //Funcion para insertar un instructor de MYSQL
    function insertar($datos1){
      //Active Record en CodeIgniter
      return $this->db->insert("estudiante",$datos1);

    }
    //Funcion para consultar Estudiantes
    function obtenerTodos(){
      $listadoEstudiantes=$this->db->get("estudiante"); //Devuelve un array   SIEMPRE VALIDAR CON UN IF
      if($listadoEstudiantes->num_rows()>0){ //SI HAY DATOS     num_rows nos deuelve el numero de filas que haya
        return $listadoEstudiantes->result();
      }else{ //NO HAY DATOS
        return false;
      }
    }

    function borrar($id_est){
      $this->db->where("id_est",$id_est);
      if ($this->db->delete("estudiante")) {
        return true;
      } else {
        return false;
      }
    }
  }// Cierre de la clase
 ?>
