<?php
  class InstructorModel extends CI_Model //CI_Model ya viene con el framework
  {
    function __construct()
    {
      // Reconocer a las clases
      parent::__construct();
    }
    //Funcion para insertar un instructor de MYSQL
    function insertar($datos){
      //Active Record en CodeIgniter
      return $this->db->insert("instructor",$datos);

    }
    //Funcion para consultar Instructores
    function obtenerTodos(){
      $listadoInstructores=$this->db->get("instructor"); //Devuelve un array   SIEMPRE VALIDAR CON UN IF
      if($listadoInstructores->num_rows()>0){ //SI HAY DATOS     num_rows nos deuelve el numero de filas que haya
        return $listadoInstructores->result();
      }else{ //NO HAY DATOS
        return false;
      }
    }
    function borrar($id_ins){
      $this->db->where("id_ins",$id_ins);
      if ($this->db->delete("instructor")) {
        return true;
      } else {
        return false;
      }
    }
  }// Cierre de la clase
 ?>
